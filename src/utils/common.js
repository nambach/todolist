const emptyFunc = () => { };
export const showConfirm = (message, okCallback = emptyFunc) => {
  if (window.confirm(message)) {
    okCallback();
  }
}