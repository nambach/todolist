import React, { Component } from 'react';
import { connect } from 'react-redux';
import { VisibilityFilters, toggleTodo, addTodo } from './todos.action';
import FilterLink from '../link/link.component';
import TodoItem from '../todo/todo.component';

class Todos extends Component {

  render() {
    let { focusTodo, todos } = this.props;

    return (
      <div style={{ textAlign: 'center' }}>
        <br />
        <form
          onSubmit={(e) => {
            e.preventDefault();
            if (this.input.value.trim() === '') return;
            this.props.onButtonAddClick(this.input.value)
            this.input.value = '';
          }}
        >
          <input ref={ref => this.input = ref} />
          <button type="submit">Add</button>
        </form>
        <br />
        <div>
          {todos.map(todo => (
            <TodoItem key={todo.id} todo={todo} focus={focusTodo.id === todo.id} />
          ))}
        </div>
        <br />
        <FilterLink filter={VisibilityFilters.SHOW_ALL} content={"Show all"} />, &nbsp;
        <FilterLink filter={VisibilityFilters.SHOW_COMPLETED} content={"Show completed"} />, &nbsp;
        <FilterLink filter={VisibilityFilters.SHOW_INCOMPLETED} content={"Show incompleted"} />
      </div>
    )
  }
}

const getVisibleTodoList = (todos, visibilityFilter) => {
  switch (visibilityFilter) {
    case VisibilityFilters.SHOW_ALL:
      return todos;

    case VisibilityFilters.SHOW_COMPLETED:
      return todos.filter(todo => todo.completed);

    case VisibilityFilters.SHOW_INCOMPLETED:
      return todos.filter(todo => !todo.completed);
  }
}

const mapStateToProps = state => {
  let { todoList: { todos, editingTodos }, visibilityFilter } = state;

  let focusTodo = {};
  let visibleTodoList = getVisibleTodoList(todos, visibilityFilter);
  //find the first editing todo to focus on
  for (let vTodo of visibleTodoList) {
    if (editingTodos.find(eTodo => eTodo.id === vTodo.id)) {
      focusTodo = vTodo;
      break;
    }
  }
  return {
    todos: visibleTodoList,
    focusTodo
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTodoClick: (id) => {
      dispatch(toggleTodo(id));
    },
    onButtonAddClick: (text) => {
      if (text && text.trim() !== "") {
        dispatch(addTodo(text));
      }
    }
  }
}
export const TodoApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(Todos);