export const ADD_TODO = "ADD_TODO";
export const TOGGLE_TODO = "TOGGLE_TODO";
export const DELETE_TODO = "DELETE_TODO";
export const START_EDITING_TODO = "START_EDITING_TODO";
export const EDIT_TODO = "EDIT_TODO";
export const CANCEL_EDITING_TODO = "CANCEL_EDITING_TODO";
export const SUBMIT_EDITING_TODO = "SUBMIT_EDITING_TODO";
export const CHANGE_VISIBILITY = "CHANGE_VISIBILITY";

export const VisibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_COMPLETED: "SHOW_COMPLETED",
  SHOW_INCOMPLETED: "SHOW_INCOMPLETED"
};

let id = 0;
export const addTodo = (text) => {
  return {
    type: ADD_TODO,
    text,
    id: ++id
  }
}

export const toggleTodo = (id) => {
  return {
    type: TOGGLE_TODO,
    id
  }
}

export const deleteTodo = (id) => {
  return {
    type: DELETE_TODO,
    id
  }
}

export const startEditTodo = (id) => {
  return {
    type: START_EDITING_TODO,
    id
  }
}

export const editTodo = (id, text) => {
  return {
    type: EDIT_TODO,
    id,
    text
  }
}

export const cancelEditTodo = (id) => {
  return {
    type: CANCEL_EDITING_TODO,
    id
  }
}

export const submitEditTodo = (id) => {
  return {
    type: SUBMIT_EDITING_TODO,
    id
  }
}

export const changeVisibility = (filter) => {
  return {
    type: CHANGE_VISIBILITY,
    filter
  }
}