import { ADD_TODO, TOGGLE_TODO, VisibilityFilters, CHANGE_VISIBILITY, START_EDITING_TODO, EDIT_TODO, SUBMIT_EDITING_TODO, CANCEL_EDITING_TODO, DELETE_TODO } from "./todos.action";
import { combineReducers } from "redux";

const initialTodoList = {
  todos: [],
  editingTodos: []
}
const todoList = (state = initialTodoList, action) => {
  let { type, id, text } = action;
  let { todos, editingTodos } = state;
  let newTodos, newEditingTodos;

  switch (type) {
    case ADD_TODO:
      newTodos = [
        ...todos,
        {
          id,
          text,
          completed: false
        }
      ];
      return {
        ...state,
        todos: newTodos
      };

    case TOGGLE_TODO:
      newTodos = todos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo,
            completed: !todo.completed
          }
        } else {
          return todo;
        }
      });
      return {
        ...state,
        todos: newTodos
      };

    case DELETE_TODO:
      return {
        ...state,
        todos: todos.filter(todo => todo.id !== id)
      };

    case START_EDITING_TODO:
      newEditingTodos = [
        ...editingTodos,
        {
          id,
          text: todos.find(todo => todo.id === id).text
        }
      ];
      return {
        ...state,
        editingTodos: newEditingTodos
      };

    case EDIT_TODO:
      newEditingTodos = editingTodos.map(eTodo => {
        if (eTodo.id === id) {
          return {
            ...eTodo,
            text
          }
        } else {
          return eTodo;
        }
      })
      return {
        ...state,
        editingTodos: newEditingTodos
      };

    case CANCEL_EDITING_TODO:
      newEditingTodos = editingTodos.filter(eTodo => eTodo.id !== id);
      return {
        ...state,
        editingTodos: newEditingTodos
      };

    case SUBMIT_EDITING_TODO:
      newTodos = todos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo,
            text: editingTodos.find(eTodo => eTodo.id === id).text
          };
        } else {
          return todo;
        }
      });
      newEditingTodos = editingTodos.filter(eTodo => eTodo.id !== id);
      return {
        ...state,
        todos: newTodos,
        editingTodos: newEditingTodos
      };

    default:
      return state;
  }
}

const visibilityFilter = (state = VisibilityFilters.SHOW_ALL, action) => {
  let { type, filter } = action;
  switch (type) {
    case CHANGE_VISIBILITY:
      return filter;

    default:
      return state;
  }
}

export const todoApp = combineReducers({
  todoList,
  visibilityFilter
});