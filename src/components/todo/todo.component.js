import React, { Component } from 'react';
import { connect } from 'react-redux';
import './todo.style.css';
import { toggleTodo, startEditTodo, editTodo, submitEditTodo, cancelEditTodo, deleteTodo } from '../todos/todos.action';
import { showConfirm } from '../../utils/common';

class Todo extends Component {

  componentDidUpdate() {
    if (this.props.focus) {
      this.todoInput.focus();
    }
  }

  renderViewMode = (todo) => {
    return (
      <div className='line-space'>
        <span
          className='no-select pointer'
          style={{
            textDecorationLine: todo.completed ? "line-through" : "none",
          }}
          onClick={() => {
            this.props.onTogglingTodo(todo.id);
          }}
        >
          {todo.text}
        </span>
        &nbsp;
        <span role='img'
          className='no-select pointer'
          onClick={() => {
            this.props.onStartEditingTodo(todo.id);
          }}
        >
          ✏️
          </span>
        <span role='img'
          className='no-select pointer'
          onClick={() => showConfirm(`Are you sure to delete "${todo.text}"?`, () => this.props.onDeleteTodo(todo.id))}
        >
          🗑️
          </span>
      </div>
    )
  }

  renderEditingMode = (todo) => {
    return (
      <div className='line-space'>
        <form onSubmit={(e) => {
          e.preventDefault();
          this.props.onSubmitEditingTodo(todo.id)
        }}>
          <input
            defaultValue={todo.text}
            ref={ref => this.todoInput = ref}
            onInput={() => this.props.onEditingTodo(todo.id, this.todoInput.value)}
          />
          <span role='img'
            className='no-select pointer'
            onClick={() => this.props.onSubmitEditingTodo(todo.id)}
          >
            💾
          </span>
          <span role='img'
            className='no-select pointer'
            onClick={() => this.props.onCancelEditingTodo(todo.id)}
          >
            ❌
        </span>
        </form>
      </div>
    )
  }

  render() {
    let { todo, editing } = this.props;
    if (editing) {
      return this.renderEditingMode(todo);
    } else {
      return this.renderViewMode(todo);
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  let { todoList: { editingTodos } } = state;
  let editingTodo = editingTodos.find(eTodo => eTodo.id === ownProps.todo.id);
  return {
    editing: editingTodo !== undefined,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onTogglingTodo: (id) => {
      dispatch(toggleTodo(id));
    },
    onDeleteTodo: (id) => {
      dispatch(deleteTodo(id));
    },
    onStartEditingTodo: (id) => {
      dispatch(startEditTodo(id));
    },
    onEditingTodo: (id, text) => {
      dispatch(editTodo(id, text));
    },
    onCancelEditingTodo: (id) => {
      dispatch(cancelEditTodo(id));
    },
    onSubmitEditingTodo: (id) => {
      dispatch(submitEditTodo(id));
    }
  }
}

const TodoItem = connect(
  mapStateToProps,
  mapDispatchToProps
)(Todo);

export default TodoItem;