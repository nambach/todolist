import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeVisibility } from '../todos/todos.action';

class Link extends Component {

  render() {
    let { active, content, onClick, filter } = this.props;
    if (active) {
      return (
        <span>{content}</span>
      )
    } else {
      return (
        <a href="#" onClick={() => onClick(filter)}>
          {content}
        </a>
      )
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  let { visibilityFilter } = state;
  return {
    active: visibilityFilter === ownProps.filter
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onClick: (filter) => {
      dispatch(changeVisibility(filter));
    }
  }
}

const FilterLink = connect(
  mapStateToProps,
  mapDispatchToProps
)(Link);

export default FilterLink;