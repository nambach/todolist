import React from 'react';
import { Provider } from 'react-redux'
import logo from './logo.svg';
import './App.css';
import { TodoApp } from './components/todos/todos.component';
import { createStore } from 'redux';
import { todoApp } from './components/todos/todos.reducer';
import AppRouter from './components/demo/route/basicRoute.component';
import NestedApp from './components/demo/route/nestingRoute.component';

function App() {
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>

    <Provider store={createStore(todoApp)}>
      <TodoApp />
    </Provider>

    // <NestedApp />
  );
}

export default App;
